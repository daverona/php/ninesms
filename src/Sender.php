<?php

namespace daVerona\Texting\NineSms;

use daVerona\Texting\Base\Sender as BaseSender;
use Requests;

class Sender extends BaseSender
{
  private const SERVER_ENDPOINT = "http://www.sms9.co.kr/authSendApi/authSendApi_UTF8.php";

  /* required fields */
  private const USER_FIELD = "sUserid";
  private const SECRET_FIELD = "authKey";
  private const SENDER_FIELD = "callNum";
  private const MODE_FIELD = "sMode";  // possible values: Real, Test
  private const RECEIVER_FIELD = "destNum";
  private const BODY_FIELD = "sendMsg";

  /* optional fields */
  private const TYPE_FIELD = "sType";  // possible values: SMS, LMS
  private const SUBJECT_FIELD = "sSubject";  // only used for LMS
  private const DATE_FIELD = "sendDate";  // send immediately if the value is present or past

  /* service provider return code */
  const CODE_SUCCESS = "0000";
  const CODE_INVALID_SENDER_NUMBER = "0011";
  const CODE_INVALID_RECEIVER_NUMBER = "0022";
  const CODE_INVALID_RESERVATION = "0033";
  const CODE_NOT_ENOUGH_CREDIT = "0044";
  const CODE_BLOCKED_AS_SPAMMER = "0055";
  const CODE_INVALID_ARGUMENTS = "0066";
  const CODE_AUTHENTICATION_FAILURE = "0077";
  const CODE_INVALID_SECRET = "0088";
  const CODE_BLOCKED_IPADDRESS = "0099";
  const CODE_UNREGISTERED_SENDER_NUMBER = "0555";
  const CODE_BLOCKED_SENDER_NUMBER = "0556";
  const CODE_UNKNOWN_ERROR = "9999";

  protected function request($receiver, $body, $subject="", $date="", $type="SMS", $mode="Real") {
    $headers = array(
    );
    $data = array(
      self::USER_FIELD => $this->credentials["user"],
      self::SECRET_FIELD => $this->credentials["secret"],
      self::TYPE_FIELD => $type,
      self::MODE_FIELD => $mode,
      self::SENDER_FIELD => $this->options["sender"],
      self::RECEIVER_FIELD => $receiver,
      self::SUBJECT_FIELD => $subject,
      self::BODY_FIELD => $body,
      self::DATE_FIELD => $date,
    );
    $response = Requests::post(self::SERVER_ENDPOINT, $headers, $data);
    return $response;
  }

  protected function respond($response) {
    if ($response->status_code <= 299) {
      $message = strip_tags($response->body);
      $message = explode("|", $message);
      $code = $message[0]; 

      switch ($code) {
        case self::CODE_INVALID_SENDER_NUMBER:
          throw new TextingException(); 
          break;
        case self::CODE_INVALID_RECEIVER_NUMBER:
          throw new TextingException(); 
          break;
        case self::CODE_INVALID_RESERVATION:
          throw new TextingException(); 
          break;
        case self::CODE_NOT_ENOUGH_CREDIT:
          throw new TextingException(); 
          break;
        case self::CODE_BLOCKED_AS_SPAMMER:
          throw new TextingException(); 
          break;
        case self::CODE_INVALID_ARGUMENTS:
          throw new TextingException(); 
          break;
        case self::CODE_AUTHENTICATION_FAILURE:
          throw new TextingException(); 
          break;
        case self::CODE_INVALID_SECRET:
          throw new TextingException(); 
          break;
        case self::CODE_BLOCKED_IPADDRESS:
          throw new TextingException(); 
          break;
        case self::CODE_UNREGISTERED_SENDER_NUMBER:
          throw new TextingException(); 
          break;
        case self::CODE_BLOCKED_SENDER_NUMBER:
          throw new TextingException(); 
          break;
        case self::CODE_UNKNOWN_ERROR:
          throw new TextingException(); 
          break;
      }
    }
  }

  public function __construct($credentials, $options) {
    assert(array_key_exists("user", $credentials) && !empty($credentials["user"])); 
    assert(array_key_exists("secret", $credentials) && !empty($credentials["secret"])); 
    assert(array_key_exists("sender", $options) && !empty($options["sender"])); 

    $this->credentials = $credentials;
    $this->options = $options;
  }

  public function signin($options) {
    return true;
  }

  public function signout($options) {
    return true;
  }

  public function dryrun($data) {
    assert(array_key_exists("receiver", $data) && !empty($data["receiver"])); 
    assert(array_key_exists("body", $data) && !empty($data["body"])); 

    $receiver = $data["receiver"];
    $body = $data["body"];
    $subject = array_key_exists("subject", $data) ? $data["subject"] : "";
    $date = array_key_exists("date", $data) ? $data["date"] : "";
    $type = array_key_exists("type", $data) ? $data["type"] : "SMS";
    $mode = array_key_exists("mode", $data) ? $data["mode"] : "Test";
    $response = $this->request($receiver, $body, $subject, $date, $type, $mode);
    return $this->respond($response);
  }

  public function send($data) {
    $data["mode"] = "Real";
    return $this->dryrun($data);
  }

  public function cancel($data) {
    return false;
  }

  public function stats($options) {
    return array();
  }
}
